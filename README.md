# sasw-editor
Web assembly with blazor for editing markdown and html

## First steps
Clone an empty repository `sasw-editor` and open a terminal in it
```
dotnet new gitignore

dotnet new blazorwasm --name Sasw.Editor.Web --output src/Sasw.Editor.Web --no-https
dotnet new classlib --name Sasw.Editor.Application --output src/Sasw.Editor.Application

dotnet new xunit --name Sasw.Editor.Application.UnitTests --output test/Sasw.Editor.Application.UnitTests

dotnet new sln
dotnet sln add src/Sasw.Editor.Web
dotnet sln add src/Sasw.Editor.Application
dotnet sln add test/Sasw.Editor.Application.UnitTests
```